/*
 * Copyright 2018 Salling Group
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import groovy.json.JsonSlurper

/**
 * This module provides a naive, curl-based integration for the Bitbucket build
 * status API. For usage and to understand why you would want to do it this way,
 * consult the included README.
 *
 * <p>In your build script, define a global handle:
 * <pre>{@code def bbnotify = null}</pre>
 * <p>In the checkout stage, initialize <em>bbnotify</em>:
 * <pre>{@code
 *     stage('checkout') {
 *         scmVars = checkout scm
 *         bbnotify = load 'openshift/bbnotify/bbnotify.groovy'
 *         bbnotify.setScmVars(scmVars)
 *         bbnotify.authorize(client, secret)
 *     }
 * }</pre>
 *
 * <p>In every subsequent stage in which you want to post a build status to
 * Bitbucket, first mark the build in progress at the start, then mark it as one
 * of the ended states at the end; use a try-catch block:
 *
 * <pre>{@code
 *     stage('build') {
 *         try {
 *             bbnotify.inprogress('build', env.BUILD_URL)
 *             sh 'mvn -B package'
 *             bbnotify.successful('build', env.BUILD_URL)
 *         } catch (e) {
 *             bbnotify.failed('build', env.BUILD_URL)
 *             throw e
 *         }
 *     }
 *}</pre>
 */

/**
 * Extracts the repository owner and name from a Bitbucket SSH clone URL such as
 * the one exposed in {@code scmVars.GIT_URL}.
 * It is an error to supply a different kind of URL. This method is not intended
 * for external use.
 *
 * @param cloneUrl the Bitbucket SSH clone URL
 */
String repoOf(String cloneUrl) {
    requireArg(cloneUrl, 'cloneUrl')

    // Assume SSH clone URL. Trivial to extend to HTTPS if necessary.
    if (!cloneUrl.startsWith('git@bitbucket.org:')) {
        error('Not a Bitbucket URL: ' + cloneUrl)
    }

    def start = cloneUrl.indexOf(':') + 1
    // Strip ".git"
    def end = -5

    return cloneUrl[start..end]
}

/**
 * Fails the build if the specified value is falsey.
 *
 * @param value the value to check
 * @param name the name to report {@code value} as
 */
def requireArg(def value, def name) {
    if (!value) {
        error("[bbnotify] missing required argument: ${name}")
    }
}

/**
 * Initializes this object with repository information.
 * Call this after loading the module.
 *
 * @param scmVars the return value of {@code checkout scm}
 */
def setScmVars(def scmVars) {
    requireArg(scmVars, 'scmVars')

    repo = repoOf(scmVars.GIT_URL)
    commit = scmVars.GIT_COMMIT
}

/**
 * Authorizes the current build for posting build status to Bitbucket.
 * Before calling this, call {@link #setScmVars}. Next, at the start of a stage,
 * call {@link #inprogress}.
 *
 * <p>bbnotify will automatically maintain a valid session.
 *
 * @param client the client ID
 * @param secret the client's secret
 * @see <a href="https://developer.atlassian.com/bitbucket/api/2/reference/meta/authentication"
 *      >Bitbucket API authentication</a>
 * @see #setScmVars
 * @see #inprogress
 */
def authorize(String client, String secret) {
    requireArg(client, 'client')
    requireArg(secret, 'secret')
    if (commit == null) {
        error('[bbnotify] authorize() before setScmVars()')
    }

    oauth_client = client
    oauth_secret = secret
}

/**
 * Gets a new access token.
 * @return an authorized access token
 */
def getToken() {
    echo('[bbnotify] authorizing...')

    // We need to explicitly disable tracing to hide credentials.
    def token = sh(
        script: """
set +x
curl --silent \
    -X POST \
    https://bitbucket.org/site/oauth2/access_token \
    -u '${oauth_client}:${oauth_secret}' \
    -d grant_type=client_credentials
""",
        returnStdout: true)
    return new JsonSlurper().parseText(token).access_token
}

/**
 * Posts the build state {@code state} for build key {@code key} with build URL
 * {@code url} of the commit specified with {@code setScmVars}.
 * This is a low-level method for the Bitbucket build status API that may be
 * useful as an abstraction escape hatch; prefer to use one of these
 * higher-level abstractions:
 *
 * <ul>
 * <li>{@link #notifying}</li>
 * <li>{@link #inprogress}</li>
 * <li>{@link #successful}</li>
 * <li>{@link #failed}</li>
 * <li>{@link #stopped}</li>
 * </ul>
 *
 * @param key the unique build key
 * @param state the build state of {@code key}
 * @param url the URL to the CI build
 * @see <a href="https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/commit/%7Bnode%7D/statuses/build"
 *      >Bitbucket build status API reference</a>
 */
def setState(String key, String state, String url) {
    requireArg(key, 'key')
    requireArg(state, 'state')
    requireArg(url, 'url')

    if (oauth_client == null || oauth_secret == null) {
        error('[bbnotify] post build status before authorize()')
    }

    def oauth_token = getToken()
    if (oauth_token == null) {
        error('[bbnotify] post build status with invalid token')
    }

    echo("[bbnotify] posting state '${state}' for key '${key}' of commit ${commit}")

    // We could write this in more idiomatic Groovy, but
    // 1) the httpRequest Jenkins plugin is not available;
    // 2) a curl command can be experimented with on the command line; and
    // 3) it is easier to do so if the (very simple) JSON is hard-coded.
    def out = sh(
        script: """
set +x
curl --silent \
    -X POST \
    https://api.bitbucket.org/2.0/repositories/${repo}/commit/${commit}/statuses/build \
    -H 'Authorization: Bearer ${oauth_token}' \
    -H 'Content-Type: application/json'  \
    -d '{"state":"${state}","key":"${key}","url":"${url}"}'
""",
        returnStdout: true).trim()

    if (out.contains('"error"')) {
        def json = new JsonSlurper().parseText(out)
        error('[bbnotify] ' + json['error']['message'])
    }
}

/**
 * Sets the current build status to {@code INPROGRESS} for key {@code key} with
 * build URL {@code url}.
 * Call this method at the start of a stage. At the end of a stage, follow up
 * with one of
 *
 * <ul>
 * <li>{@link #successful}</li>
 * <li>{@link #failed}</li>
 * <li>{@link #stopped}</li>
 * </ul>
 *
 * <p>This is a mid-level method. Consider using the high-level method,
 * {@link #notifying}.
 *
 * @param key the unique build key
 * @param url the URL to the CI build
 */
def inprogress(String key, String url) {
    setState(key, 'INPROGRESS', url)
}

/**
 * Sets the current build status to {@code SUCCESSFUL} for key {@code key} with
 * build URL {@code url}.
 * Call this method at the end of a successful stage.
 *
 * <p>This is a mid-level method. Consider using the high-level method,
 * {@link #notifying}.
 *
 * @param key the unique build key
 * @param url the URL to the CI build
 */
def successful(String key, String url) {
    setState(key, 'SUCCESSFUL', url)
}

/**
 * Sets the current build status to {@code FAILED} for key {@code key} with
 * build URL {@code url}.
 * Call this method at the end of a failed stage.
 *
 * <p>This is a mid-level method. Consider using the high-level method,
 * {@link #notifying}.
 *
 * @param key the unique build key
 * @param url the URL to the CI build
 */
def failed(String key, String url) {
    setState(key, 'FAILED', url)
}

/**
 * Sets the current build status to {@code STOPPED} for key {@code key} with
 * build URL {@code url}.
 * Call this method at the end of an aborted stage.
 *
 * <p>This is a mid-level method. Consider using the high-level method,
 * {@link #notifying}.
 *
 * @param key the unique build key
 * @param url the URL to the CI build
 */
def stopped(String key, String url) {
    setState(key, 'STOPPED', url)
}

/**
 * Executes a closure containing build steps, correctly setting the build status
 * before and after, also propagating the error in case of failure.
 * This is a high-level method around the mid-level methods: it
 * 
 * <ol>
 * <li>notifies of build start;
 * <li>executes the closure {@code body} containing the build steps; and
 * <li>notifies of success or failure depending on outcome.
 * </ol>
 *
 * <p>For post-processing after failure, for instance to mark the build unstable
 * and publish a Checkstyle report, wrap the invocation in try-catch-finally:
 *
 * <pre>
 *     try {
 *         bbnotify.notifying('validate', env.BUILD_URL, {
 *             sh 'mvn -B validate'
 *         })
 *     } catch (e) {
 *         currentBuild.result = 'UNSTABLE'
 *     } finally {
 *         checkstyle(unstableTotalAll: '1')
 *         publishHTML(target: [
 *             reportName: 'Checkstyle Report',
 *             reportDir: 'target/site',
 *             reportFiles: 'checkstyle-aggregate.html',
 *         ])
 *     }
 * </pre>
 *
 * <p>This is the recommended method for simple, straight-forward builds. For
 * more control, use the mid- or low-level methods.</p>
 *
 * @param key the unique build key
 * @param url the URL to the CI build
 * @param body the closure containing the steps comprising this build
 */
def notifying(String key, String url, Closure body) {
    requireArg(body, 'body')
    try {
        inprogress(key, url)
        body()
        successful(key, url)
    } catch (e) {
        failed(key, url)
        throw e
    }
}

String oauth_client = null
String oauth_secret = null
String repo = null
String commit = null

return this
